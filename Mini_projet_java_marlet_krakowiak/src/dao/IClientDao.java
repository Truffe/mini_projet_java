/*
 * Interface de la classe ClientDao 
 * 
 * 
 */

package dao;
import java.util.ArrayList;

import metier.Client;
public interface IClientDao {
	ArrayList<Client> CreateClient( );
	Client GetClientParMC( ArrayList<Client> Nos_Client, String mc);
	ArrayList<Client> DeleteClientParMC( ArrayList<Client> Nos_Client, String mc);
	
}
