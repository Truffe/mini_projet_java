package dao;

import java.util.ArrayList;
import java.util.Scanner;

import metier.Client;

public class ClientDao implements IClientDao {
	private ArrayList<Client> MesClients;

	public ClientDao () {
		this.MesClients = new ArrayList<Client>();
	}
	
	/*
	 *	Fonction pour enregistrer notre client
	 *	Un scanner permet de remplir les champs de notre client
	 *	on retourne notre client
	 */
	@Override
	public ArrayList<Client> CreateClient() {
		String nom;
		String prenom;
		String Ville;
		int age;
		Scanner sc = new Scanner(System.in);
		System.out.println("saisir Nom :");
		nom = sc.next();
		nom= nom.toLowerCase();
		System.out.println("saisir Prenom : ");
		prenom = sc.next();
		prenom = prenom.toLowerCase();
		System.out.println("saisir Ville : ");
		Ville = sc.next();
		System.out.println("saisir age : ");
		age = sc.nextInt();
		Client monClient = new Client(nom,prenom,Ville,age);
		MesClients.add(monClient);
		return this.MesClients;
	}
	
	/*
	 * Fonction pour rechercher le premier client dont son nom commence par le mot clee choisis 
	 * affiche et retourne un client
	 * retourne un client null si rien n'est trouv�
	 */
	@Override
	public Client GetClientParMC(ArrayList<Client> Nos_Client, String mc) {
		//Compare le debut du nom des clients de la liste avec le mot clee
		mc = mc.toLowerCase();
		for (Client monClient : Nos_Client) {
			String debutNomClient= "";
			for(int i = 0; i<mc.length();i++) {
				char maLettre = mc.charAt(i);
				if(monClient.getNomClient().charAt(i)== maLettre) {
					debutNomClient = debutNomClient+maLettre;
				}
				if(debutNomClient.equals(mc)) {
					System.out.println("Le client trouv� est : : "+ monClient.getNomClient()+" "+monClient.getPrenomClient());
					return monClient;
				}
			}


		}
		
		Client PasDeClient = new Client(null, null,null,0);
		System.out.println("Pas de Client trouv�");
		return PasDeClient;
	}
	/*
	 * On supprime les clients qui ont un nom commencant par notre mot clee mc
	 * On parcours notre liste de client pour cela et on compare leur debut de nom
	 */
	@Override
	public ArrayList<Client> DeleteClientParMC(ArrayList<Client> Nos_Client, String mc) {
		ArrayList<Client> Client_efface = new ArrayList<Client>();
		mc=mc.toLowerCase();
		for (Client monClient : Nos_Client) {
			int indexNosClient = 0;
			String debutNomClient = "";
			for(int i = 0; i<mc.length();i++) {
				char maLettre = mc.charAt(i);
				if(monClient.getNomClient().charAt(i)== maLettre) {
					debutNomClient = debutNomClient+maLettre;
				}
			
				
			}
			
			if(debutNomClient.equals(mc)) {
				System.out.println("removed");
				Client_efface.add(monClient);
			}
			indexNosClient++;
		}
		Nos_Client.removeAll(Client_efface);
		return Nos_Client;
	}
	/*
	 * Menu afin de choisir les actions que l'on souhaite effectuer
	 * 
	 */
	public void menu() {
		int choix = 0;
		Scanner sc = new Scanner (System.in);
		do{
			System.out.println("--- Menu du client (remontez bien la console pour voir vos affichages) ---");
			System.out.println(" 1- Enregistrez");
			System.out.println(" 2- Rechercher");
			System.out.println(" 3- Supprimer les clients");
			System.out.println(" 4- Afficher");
			System.out.println(" 0- Quitter");
			System.out.println(" Votre choix");
			choix = sc.nextInt();
			switch(choix)
			{
			case 1 : this.CreateClient();break;
			case 2 : Scanner mc = new Scanner(System.in);
					 System.out.println("saisir mot clee :");
			         String motClee = mc.next();
			         motClee=motClee.toLowerCase();
					 this.GetClientParMC(this.MesClients, motClee);
					 break;
			case 3 : 	Scanner mc2 = new Scanner(System.in);
			 			System.out.println("saisir mot clee :");
			 			String motClee2 = mc2.next();
			 			motClee2 = motClee2.toLowerCase();
			 			this.DeleteClientParMC(this.MesClients, motClee2);
			 			break;
			case 4 : 	for (Client monClient : this.MesClients) {
						System.out.println("voici les clients: ");
						System.out.println(monClient.getNomClient());
						System.out.println(monClient.getPrenomClient());
						System.out.println(monClient.getVille());
						System.out.println(monClient.getAge());
					 }break;
			
						
			}
		}while(choix !=0);
	}

}


