package dao;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.*;

import metier.Produit;
import metier.ProduitA;
import metier.ProduitB;

/*
 * Classe qui nous permet de gerer nos interfaces graphiques
 * 
 * 
 */
public class InterfaceGraphique extends JFrame implements ActionListener{
	private JLabel jlabelProduit = new JLabel("Nom Produit : ");
	private JLabel jlabelVille = new JLabel("Ville : ");
	private JLabel jlabellePrixU = new JLabel("Prix Unitaire : ");
	private JLabel jlabelQuantite = new JLabel("Quantit� : ");
	private JLabel jlabelQualite = new JLabel("Qualit� : ");
	private JLabel jlabelTauxReduc = new JLabel("Taux reduc : ");



	private JTextField texteProduit = new JTextField();
	private JTextField texteVille = new JTextField();
	private JTextField textePrixU = new JTextField();
	private JTextField texteQuantite = new JTextField();
	private JTextField texteQualite = new JTextField();
	private JTextField texteTauxReduc = new JTextField();


	private JButton btValide = new JButton("Ajout Produit");
	private JButton btrenit = new JButton("Afficher Produit ");
	private JButton btquit = new JButton("Quitter");
	private JButton A = new JButton("Ajout pA");
	private JButton B = new JButton("Ajout pB");
	private JButton BoutonClient = new JButton("Lancer partie Client");


	private ArrayList<Produit> NosProduits = new ArrayList<Produit>(); 
	private ProduitA produitA;
	private ProduitB produitB;

	

	
	private int choix;

	/*
	 * Mes diff�rentes interfaces graphiques suivant quel produit on veut ajouter
	 * 
	 */
	public InterfaceGraphique(int x,ArrayList<Produit> NosProduits) {
		//initialisation du jeu
		if(x==2) {
			this.choix = x;
			this.NosProduits = NosProduits;
			this.setBounds(200, 200, 700, 600);
			this.setTitle("Affichage produit B");
			this.setResizable(false);
			this.getContentPane().setBackground(Color.WHITE);
			//enl�ve le style quadrillage de la frame
			this.setLayout(null);
			/*
			 *  Affichage des boutons et zone de texte
			 */
			// 1
			this.jlabelProduit.setBounds(50,20,100,20);
			this.add(this.jlabelProduit);

			this.texteProduit.setBounds(50,50,350,20);
			this.add(this.texteProduit);
			// 2
			this.jlabelVille.setBounds(50,90,100,20);
			this.add(this.jlabelVille);

			this.texteVille.setBounds(50,130,350,20);
			this.add(this.texteVille);

			// 3
			this.jlabellePrixU.setBounds(50,160,100,20);
			this.add(this.jlabellePrixU);

			this.textePrixU.setBounds(50,200,350,20);
			this.add(this.textePrixU);
			// 4
			this.jlabelQuantite.setBounds(50,240,100,20);
			this.add(this.jlabelQuantite);

			this.texteQuantite.setBounds(50,280,350,20);
			this.add(this.texteQuantite);
			/*
				this.jlabelQualite.setBounds(50,320,100,20);
				this.add(this.jlabelQualite);

				this.texteQualite.setBounds(50,360,350,20);
				this.add(this.texteQualite);

			 */
			//6
			this.jlabelTauxReduc.setBounds(50,400,100,20);
			this.add(this.jlabelTauxReduc);

			this.texteTauxReduc.setBounds(50,440,350,20);
			this.add(this.texteTauxReduc);
			//bouton ajouter
			this.btValide.setBounds(50, 500, 100, 20);
			this.add(this.btValide);
			//bouton afficher
			this.btrenit.setBounds(200, 500, 100, 20);
			this.add(this.btrenit);
			// bouton quitter
			this.btquit.setBounds(450, 500, 100, 20);
			this.add(this.btquit);

			//rendre le bton �coutable
			this.btValide.addActionListener(this);
			this.btrenit.addActionListener(this);
			this.btquit.addActionListener(this);


			this.setVisible(true);
		}
		if(x==1) {
			this.choix=x;
			this.NosProduits = NosProduits;
			this.setBounds(200, 200, 700, 600);
			this.setTitle("Affichage produit A");
			this.setResizable(false);
			this.getContentPane().setBackground(Color.WHITE);
			//enl�ve le style quadrillage de la frame
			this.setLayout(null);
			/*
			 *  Affichage des boutons et zone de texte
			 */
			// 1
			this.jlabelProduit.setBounds(50,20,100,20);
			this.add(this.jlabelProduit);

			this.texteProduit.setBounds(50,50,350,20);
			this.add(this.texteProduit);
			// 2
			this.jlabelVille.setBounds(50,90,100,20);
			this.add(this.jlabelVille);

			this.texteVille.setBounds(50,130,350,20);
			this.add(this.texteVille);

			// 3
			this.jlabellePrixU.setBounds(50,160,100,20);
			this.add(this.jlabellePrixU);

			this.textePrixU.setBounds(50,200,350,20);
			this.add(this.textePrixU);
			// 4
			this.jlabelQuantite.setBounds(50,240,100,20);
			this.add(this.jlabelQuantite);

			this.texteQuantite.setBounds(50,280,350,20);
			this.add(this.texteQuantite);
			// 5
			this.jlabelQualite.setBounds(50,320,100,20);
			this.add(this.jlabelQualite);

			this.texteQualite.setBounds(50,360,350,20);
			this.add(this.texteQualite);
			/*
				this.jlabelTauxReduc.setBounds(50,400,100,20);
				this.add(this.jlabelTauxReduc);

				this.texteTauxReduc.setBounds(50,440,350,20);
				this.add(this.texteTauxReduc);
			 */
			this.btValide.setBounds(50, 500, 100, 20);
			this.add(this.btValide);

			this.btrenit.setBounds(200, 500, 100, 20);
			this.add(this.btrenit);

			this.btquit.setBounds(450, 500, 100, 20);
			this.add(this.btquit);

			//rendre le bton �coutable
			this.btValide.addActionListener(this);
			this.btrenit.addActionListener(this);
			this.btquit.addActionListener(this);


			this.setVisible(true);

		} 
		/*
		 * interface graphique de mon produit B sous forme d'un fenetre avec un tableau de mon produit
		 * 
		 */
		if(x==3) {
			this.choix=x;
			this.NosProduits = NosProduits;
			JFrame frame = new JFrame("Table produit B");
			String[] columns = new String[] {
					"Nom","Ville","PrixUnitaire","Quantite","TauxReduc","PrixReduitTotal"
			};

			Object[][] data2 = new Object[][] {
				{	

				},

			};
			//donn�es pour JTable dans un tableau 2D
			if(this.NosProduits.get(NosProduits.size()-1) instanceof ProduitB) {
				ProduitB prodb = new ProduitB(	this.NosProduits.get(NosProduits.size()-1).getNom(),
						this.NosProduits.get(NosProduits.size()-1).getVille(),
						this.NosProduits.get(NosProduits.size()-1).getPrixU(),
						this.NosProduits.get(NosProduits.size()-1).getQuantite(),
						((ProduitB)this.NosProduits.get(NosProduits.size()-1)).getTauxReduc()

						);

				Object[][] data3 = new Object[][] {
					{	prodb.getNom(), 
						prodb.getVille(),
						prodb.getPrixU(),
						prodb.getQuantite(),
						prodb.getTauxReduc(),
						prodb.calculPrixReduit()

					},


				};
				data2 = data3;
			}



			//cr�e un JTable avec des donn�es
			JTable table = new JTable(data2, columns);

			JScrollPane scroll = new JScrollPane(table);
			table.setFillsViewportHeight(true);

			JLabel labelHead = new JLabel("Voici mon ProduitB");
			labelHead.setFont(new Font("Arial",Font.TRUETYPE_FONT,20));

			frame.getContentPane().add(labelHead,BorderLayout.PAGE_START);
			//ajouter la table au frame
			frame.getContentPane().add(scroll,BorderLayout.CENTER);


			frame.setSize(500, 200);
			frame.setVisible(true);
		}
		/*
		 * interface graphique de mon produit A sous forme d'un fenetre avec un tableau de mon produit
		 * 
		 */
		if(x==4) {
			this.choix = x;
			this.NosProduits = NosProduits;
			JFrame frame = new JFrame("Table produit A");
			String[] columns = new String[] {
					"Nom","Ville","PrixUnitaire","Quantite","Qualite","PrixTotal"
			};


			//donn�es pour JTable dans un tableau 2D

			Object[][] data2 = new Object[][] {
				{	this.NosProduits.get(NosProduits.size()-1).getNom(), 
					this.NosProduits.get(NosProduits.size()-1).getVille(),
					this.NosProduits.get(NosProduits.size()-1).getPrixU(), 
					this.NosProduits.get(NosProduits.size()-1).getQuantite(),
					((ProduitA)this.NosProduits.get(NosProduits.size()-1)).getQualite(),
					this.NosProduits.get(NosProduits.size()-1).CalculePrix() },

			};
			

			//cr�e un JTable avec des donn�es
			JTable table = new JTable(data2, columns);

			JScrollPane scroll = new JScrollPane(table);
			table.setFillsViewportHeight(true);

			JLabel labelHead = new JLabel("Voici mon ProduitA");
			labelHead.setFont(new Font("Arial",Font.TRUETYPE_FONT,20));

			frame.getContentPane().add(labelHead,BorderLayout.PAGE_START);
			//ajouter la table au frame
			frame.getContentPane().add(scroll,BorderLayout.CENTER);


			frame.setSize(500, 200);
			frame.setVisible(true);
		}

	}
	/*
	 * Premiere interface lanc�e dans le main
	 * 
	 * 
	 */
	public InterfaceGraphique (String choix) {	
		//initialisation du jeu
		this.NosProduits = NosProduits;
		this.setBounds(200, 200, 700, 600);
		this.setTitle("Selectionnez un produit a ajouter ou lancez la partie client sur console");
		this.setResizable(false);
		this.getContentPane().setBackground(Color.WHITE);
		//enl�ve le style quadrillage de la frame
		this.setLayout(null);
		/*
		 *  Affichage des boutons et zone de texte
		 */




		this.A.setBounds(50, 200, 140, 20);
		this.add(this.A);

		this.B.setBounds(200, 200, 140, 20);
		this.add(this.B);

		this.btquit.setBounds(400, 200, 140, 20);
		this.add(this.btquit);

		this.BoutonClient.setBounds(50,350,180,20);
		this.add(this.BoutonClient);

		//rendre le bton �coutable
		this.A.addActionListener(this);
		this.B.addActionListener(this);
		this.btquit.addActionListener(this);
		this.BoutonClient.addActionListener(this);


		this.setVisible(true);
	}

	

	/*
	 * 
	 * Methode qui g�re mes actions suivant le bouton utilis�
	 * 
	 */
	public void actionPerformed(ActionEvent e) {
		int boutPrixU = -1;
		int boutQuantite = -1;
		String boutProduit= "";
		String boutVille="";
		String boutQualite="";
		int boutTauxReduc =-1;
		
		// si on appuie sur quitters
		
		if(e.getSource()==this.btquit) {
			System.exit(0);
		}
		/*
		 * si on appuie sur le bouton client, cela lance la partie Client
		 * On va pouvoir ajouter nos clients
		 */
		if(e.getSource()==this.BoutonClient) {
			this.dispose();
			JOptionPane.showMessageDialog(this, "Veuillez g�rer la partie client sur la console");
			ClientDao client = new ClientDao();
			client.menu();
			System.exit(0);


		}
		/*
		 * ici on peux choisir soit d'ajouter un produit A soit un Produit B
		 * 
		 */
		if(e.getSource()==this.A)	{
			this.dispose();
			InterfaceGraphique jeu2 = new InterfaceGraphique(1,this.NosProduits);
			//jeu2.setVisible(true);
			JOptionPane.showMessageDialog(this, "Vous allez ajouter un produit A");
		}
		if(e.getSource()==this.B)	{
			this.dispose();
			InterfaceGraphique jeu2 = new InterfaceGraphique(2,this.NosProduits);
			//jeu2.setVisible(true);
			JOptionPane.showMessageDialog(this, "Vous allez ajouter un produit B");
		}

		/*
		 * Quelques petites v�rifications simples pour que l'utilisateur rentre
		 * les informations correctements dans la zone de texte
		 */
		if(this.choix==2 && !this.textePrixU.getText().equals("") && !this.texteTauxReduc.getText().equals("")) {
			boutPrixU = Integer.parseInt(this.textePrixU.getText());
			boutQuantite = Integer.parseInt(this.texteQuantite.getText());
			boutProduit = this.texteProduit.getText().toString();
			boutVille = this.texteVille.getText().toString();
			//boutQualite = this.texteQualite.getText().toString();
			boutTauxReduc = Integer.parseInt(this.texteTauxReduc.getText());
		}
		if(this.choix==1 && !this.textePrixU.getText().equals("")) {
			if(	!this.texteQualite.getText().toString().equals("haut de gamme") &&
					!this.texteQualite.getText().toString().equals("bas de gamme")) 
			{	

				this.dispose();
				JOptionPane.showMessageDialog(this, "remplir qualite par soit [bas de gamme] soit [haut de game]");



			}
			else {
				boutPrixU = Integer.parseInt(this.textePrixU.getText());
				boutQuantite = Integer.parseInt(this.texteQuantite.getText());
				boutProduit = this.texteProduit.getText().toString();
				boutVille = this.texteVille.getText().toString();
				boutQualite = this.texteQualite.getText().toString();
			}
		}

		/*
		 *  Si on appuie sur le bouton valider
		 *  le programme refait quelques v�rifications pour voir si les champs sont bien remplis
		 *  Si tout est ok, il ajoute le produit dans une ArrayList et affiche un message de succ�s
		 *  Sinon il retourne sur la page de menu
		 */


		if (e.getSource() == this.btValide) {
			System.out.println("voici choix 1ier: "+this.choix);
			if(this.choix==1 && boutQualite.equals("")) {
				JOptionPane.showMessageDialog(this, "veuillez remplir correctement les champs du produit A, on relance");
				this.dispose();
				InterfaceGraphique jeu = new InterfaceGraphique("lance");

			}
			else if(this.choix==2 && boutTauxReduc<0 ) {
				System.out.println("voici choix dans b: "+this.choix);
				JOptionPane.showMessageDialog(this, "veuillez remplir correctement les champs du produit B, on relance");
				this.dispose();
				InterfaceGraphique jeu = new InterfaceGraphique("lance");
			}
			else if(this.choix==1) {
				JOptionPane.showMessageDialog(this, "Votre ajout " + "-"+
						boutPrixU+"-"+boutQuantite+"-"+boutProduit+"-"+boutVille+boutQualite 
						+ "Info message: "+
						JOptionPane.INFORMATION_MESSAGE);
				if(boutTauxReduc==-1) {


					ProduitA Produit = new ProduitA(boutProduit,boutVille,boutPrixU,boutQuantite,boutQualite);

					this.NosProduits.add(Produit);
					InterfaceGraphique newProduitA = new InterfaceGraphique(1,this.NosProduits);

					this.dispose();

				}
			}
			else{
				JOptionPane.showMessageDialog(this, "Votre ajout: " +
						boutPrixU+"-"+boutQuantite+"-"+boutProduit+"-"+boutVille+boutTauxReduc 
						+ "info message "+
						JOptionPane.INFORMATION_MESSAGE);
				this.produitB = new ProduitB(boutProduit,boutVille,boutPrixU,boutQuantite,boutTauxReduc);
				System.out.println(boutTauxReduc);
				this.NosProduits.add(produitB);
				InterfaceGraphique ProduitB = new InterfaceGraphique(2,this.NosProduits);
				this.dispose();
			}
		}
		/*
		 * Ici, on appuie sur le bouton d'affichage du produit
		 * Apr�s quelques v�rifications, le programme lance une nouvelle fenetre avec notre produit
		 * Affich� sous forme de tableau
		 * 
		 */
		if(e.getSource()==this.btrenit && this.NosProduits.size()==0) {
			JOptionPane.showMessageDialog(this, "veuillez remplir correctement les champs du produit");
		}

		if(e.getSource()==this.btrenit && this.NosProduits.size()>0 )	{
			if(this.choix==2) {
				System.out.println(this.NosProduits);
				InterfaceGraphique jeu2 = new InterfaceGraphique(3,this.NosProduits);
				//jeu2.setVisible(true);
				JOptionPane.showMessageDialog(this, "Affichage de votre produit");
			}
			else {
				System.out.println(this.NosProduits);
				InterfaceGraphique jeu2 = new InterfaceGraphique(4,this.NosProduits);
				//jeu2.setVisible(true);
				JOptionPane.showMessageDialog(this, "Affichage de votre produit");
			}
		}
	}
}
