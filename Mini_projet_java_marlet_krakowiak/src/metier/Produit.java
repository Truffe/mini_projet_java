package metier;

import java.util.ArrayList;
/*
 * Classe m�re Produit qui a comme fille ProduitA et ProduitB
 * 
 * 
 */
public class Produit implements IProduit {
	public String Ville;
	public String Nom;
	public int PrixU;
	public int Quantite;
	private ArrayList<Produit> mesProduits;
	
	public Produit (String Ville, String Nom, int PrixU, int Quantite) {
		this.Ville = Ville;
		this.Nom=Nom;
		this.PrixU=PrixU;
		this.Quantite = Quantite;
	}
	public Produit () {
		this.Ville = "";
		this.Nom="";
		this.PrixU=-1;
		this.Quantite = -1;
	}
	
	public String getVille() {
		return Ville;
	}

	public void setVille(String ville) {
		Ville = ville;
	}

	public String getNom() {
		return Nom;
	}

	public void setNom(String nom) {
		Nom = nom;
	}

	public int getPrixU() {
		return PrixU;
	}

	public void setPrixU(int prixU) {
		PrixU = prixU;
	}

	public int getQuantite() {
		return Quantite;
	}

	public void setQuantite(int quantite) {
		Quantite = quantite;
	}

	public ArrayList<Produit> getMesProduits() {
		return mesProduits;
	}

	public void setMesProduits(ArrayList<Produit> mesProduits) {
		this.mesProduits = mesProduits;
	}

	@Override
	public int CalculePrix() {
		return this.PrixU*this.Quantite;
	}
	
	public void AddProduit (Produit monProduit) {
		this.mesProduits.add(monProduit);
	}
	
}
