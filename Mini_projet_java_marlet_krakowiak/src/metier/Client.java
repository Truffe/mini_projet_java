package metier;

import java.util.ArrayList;

public class Client {
	private String NomClient;
	private String PrenomClient;
	private String Ville;
	private int age;
	
	
	
	public Client(String nomClient, String prenomClient, String Ville, int age) {
		this.NomClient = nomClient;
		this.PrenomClient = prenomClient;
		this.Ville = Ville;
		this.age = age;
	
	}
	
	public String getNomClient() {
		return NomClient;
	}
	public void setNomClient(String nomClient) {
		NomClient = nomClient;
	}
	public String getPrenomClient() {
		return PrenomClient;
	}
	public void setPrenomClient(String prenomClient) {
		PrenomClient = prenomClient;
	}

	public String getVille() {
		return Ville;
	}

	public void setVille(String ville) {
		Ville = ville;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
}
