package metier;
/*
 * 
 * Classe fille de Produit
 * 
 */
public class ProduitA extends Produit {
	public String Qualite;
	
	public ProduitA(String Ville, String Nom, int PrixU, int Quantite, String Qualite) {
		super(Ville, Nom, PrixU, Quantite);
		this.Qualite = Qualite;
		
	}

	public String getQualite() {
		return Qualite;
	}

	public void setQualite(String qualite) {
		Qualite = qualite;
	}

	
}
