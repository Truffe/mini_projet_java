package metier;
/*
 * 
 * Classe fille de produit
 * 
 */
public class ProduitB extends Produit  {
	public int TauxReduc;
	
	public ProduitB(String Ville, String Nom, int PrixU, int Quantite, int TauxReduc) {
		super(Ville, Nom, PrixU, Quantite);
		this.TauxReduc= TauxReduc;
		
	}
	
	
	public int getTauxReduc() {
		return this.TauxReduc;
	}


	public void setTauxReduc(int tauxReduc) {
		this.TauxReduc = tauxReduc;
	}


	public double calculPrixReduit() {
		
		return this.Quantite*this.PrixU-(this.Quantite*this.PrixU*(this.TauxReduc/100.));
		
	}
	
}
